Requisitos Previos
- PHP 7.3
- Composer 1.8.5
- Apache o algun otro server pero con la regla Rewrite habilitada

Para instalarlo seguir los siguientes pasos: 
- Clona el repositorio
- Crea una base de datos y restaura el backup lg_nano.sql 
- Edita el archivo admin/.env y pon las credenciales de la base de datos
- Modifica los permisos a la carpeta admin/storage con el comando chmod -R 777 admin/storage
- Dirigete a la carpeta admin y ejecuta el comando composer dump-autoload

Finalmente prueba la pagina web
Para acceder al administrador de los votos entra a la ruta admin/public/admin/login

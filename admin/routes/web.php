<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Personas;
use App\Opcion;
use App\Votos;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('html',function(){
    $slider = DB::table('Slider')
            ->select('name', 'photo')
            ->get();
    
    $options = DB::table('lg_options')
            ->select('id','name', 'file1','file2','file3')
            ->get();
    
    $departments = DB::table('Departments')
            ->select('id','name', 'ubigeo')
            ->get();
    $cities = DB::table('Cities')
            ->select('id','name', 'ubigeo')
            ->get();
    $districts = DB::table('Districts')
            ->select('id','name', 'ubigeo')
            ->get();
    $interest = DB::table('Interests')
            ->select('id','name')
            ->get();
    $interest_size = DB::table('Interests Size')
            ->select('id','name')
            ->get();


    $devolver = [
        "slider" => $slider,
        "options" => $options,
        "departments" => $departments,
        "cities" => $cities,
        "districts" => $districts,
        "interests" => $interest,
        "interests_size" => $interest_size
    ];

    echo json_encode($devolver);
});

Route::post('person',function(Request $request){
    $data = $request->all();
    $data["fullname"] = $data["name"]." ".$data["lastname"];
    try{
        if(sizeof(Personas::where("dni",$data["dni"])->get()) > 0){
            return response("",401);
        }
        else{
            $persona = Personas::create($request->all());
            $persona->fullname = $data["name"]." ".$data["lastname"];
            $persona->save();
            $voto = new Votos();
            $voto->opcion_id = $data["opcion_id"];
            $voto->person_id = $persona->id;
            $voto->save();

            $opcionimagen = $data["opcion_imagen_id"];

            $opcion = Opcion::find($data["opcion_id"]);
            $opcion->nro_votos = $opcion->nro_votos +1;

            if(strpos($opcionimagen,"_1") == 1)
                $opcion->nro_votos_primera = $opcion->nro_votos_primera +1;
            else if(strpos($opcionimagen,"_2") == 1)
                $opcion->nro_votos_segunda = $opcion->nro_votos_segunda +1;
            else if(strpos($opcionimagen,"_3") == 1)
                $opcion->nro_votos_tercera = $opcion->nro_votos_tercera +1;

            $opcion->save();
            return response("",200);
        }    
    }
    catch(\Exception $e){        
        echo $e->getMessage();
    }
});
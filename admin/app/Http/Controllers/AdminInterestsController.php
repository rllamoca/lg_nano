<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminInterestsController extends CBController {


    public function cbInit()
    {
        $this->setTable("Interests");
        $this->setPermalink("interests");
        $this->setPageTitle("Interests");

        $this->addDatetime("Creado el","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addText("Nombre","name")->strLimit(150)->maxLength(255);
		$this->addDatetime("Actualizado el","updated_at")->required(false)->showAdd(false)->showEdit(false);
		

    }
}

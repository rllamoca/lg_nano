<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminSliderController extends CBController {


    public function cbInit()
    {
        $this->setTable("Slider");
        $this->setPermalink("slider");
        $this->setPageTitle("Slider");

        $this->addDatetime("Creado el","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addText("Nombre","name")->strLimit(150)->maxLength(255);
		$this->addImage("Foto","photo")->encrypt(true);
		$this->addDatetime("Actualizado el","updated_at")->required(false)->showAdd(false)->showEdit(false);
		

    }
}

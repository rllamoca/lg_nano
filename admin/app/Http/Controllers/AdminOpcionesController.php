<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;
use App\Http\Controllers\AdminVotosController;
use DB;

class AdminOpcionesController extends CBController {


    public function cbInit()
    {
        $this->setTable("lg_options");
        $this->setPermalink("opciones");
        $this->setPageTitle("Opciones");

		$this->addText("Nombre","name")->strLimit(150)->maxLength(255);
		$this->addImage("Primera Imagen","file1")->encrypt(true);
		$this->addImage("Segunda Imagen","file2")->encrypt(true);
		$this->addImage("Tercera Imagen","file3")->encrypt(true);
		$this->addText("Nro Votos","nro_votos");
		$this->addText("Nro Votos Primera Imagen","nro_votos_primera");
		$this->addText("Nro Votos Segunda Imagen","nro_votos_segunda");
		$this->addText("Nro Votos Tercera Imagen","nro_votos_tercera");
        $this->addDatetime("Creado el","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Actualizado el","updated_at")->required(false)->showAdd(false)->showEdit(false);
        

        /*$this->hookIndexQuery(function($query) {
            // Todo: code query here
    
            // You can make query like laravel db builder
            //echo json_encode($query); exit;
    
            // Don't forget to return back
            return $query;
        });*/
    
    }
}

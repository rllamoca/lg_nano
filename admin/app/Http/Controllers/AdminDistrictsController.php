<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminDistrictsController extends CBController {


    public function cbInit()
    {
        $this->setTable("Districts");
        $this->setPermalink("districts");
        $this->setPageTitle("Districts");

        $this->addDatetime("Creado el","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addText("Nombre","name")->strLimit(150)->maxLength(255);
		$this->addText("Ubigeo","ubigeo")->strLimit(150)->maxLength(255);
		$this->addDatetime("Actualizado el","updated_at")->required(false)->showAdd(false)->showEdit(false);
		

    }
}

<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminInterestsSizeController extends CBController {


    public function cbInit()
    {
        $this->setTable("Interests Size");
        $this->setPermalink("interests_size");
        $this->setPageTitle("Interests Size");

        $this->addDatetime("Creado el","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addText("Nombre","name")->strLimit(150)->maxLength(255);
		$this->addDatetime("Actualizado el","updated_at")->required(false)->showAdd(false)->showEdit(false);
		

    }
}

<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminVotosController extends CBController {


    public function cbInit()
    {
        $this->setTable("Votos");
        $this->setPermalink("votos");
        $this->setPageTitle("Votos");

        $this->addDatetime("Creado el","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addSelectTable("Opcion","opcion_id",["table"=>"lg_options","value_option"=>"id","display_option"=>"name","sql_condition"=>""]);
		$this->addSelectTable("Persona","person_id",["table"=>"Personas","value_option"=>"id","display_option"=>"fullname","sql_condition"=>""]);
		$this->addDatetime("Actualizado el","updated_at")->required(false)->showAdd(false)->showEdit(false);
		

    }
}

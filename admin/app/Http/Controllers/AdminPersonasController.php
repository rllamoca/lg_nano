<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminPersonasController extends CBController {


    public function cbInit()
    {
        $this->setTable("Personas");
        $this->setPermalink("personas");
        $this->setPageTitle("Personas");

        //$this->addText("Dirección","address")->strLimit(150)->maxLength(255);
		$this->addText("Nombres Completos","fullname")->strLimit(150)->maxLength(255);
		$this->addNumber("Fec. Nacimiento","birthdate");
		$this->addSelectTable("Departamento","department",["table"=>"Departments","value_option"=>"id","display_option"=>"name","sql_condition"=>""]);
		$this->addSelectTable("Ciudad","city",["table"=>"Cities","value_option"=>"id","display_option"=>"name","sql_condition"=>""])->foreignKey('department');
		$this->addSelectTable("Distrito","district",["table"=>"Districts","value_option"=>"id","display_option"=>"name","sql_condition"=>""]);
		$this->addText("Dni","dni")->strLimit(150)->maxLength(8);
		$this->addEmail("Email","email");
		$this->addSelectTable("Interes","interest_id",["table"=>"Interests","value_option"=>"id","display_option"=>"name","sql_condition"=>""]);
		$this->addSelectTable("Tamaño de Interés","interest_size",["table"=>"Interests Size","value_option"=>"id","display_option"=>"name","sql_condition"=>""]);
		$this->addText("Teléfono","phone")->strLimit(150)->maxLength(12);
		/*$this->addText("Nombres","name")->strLimit(150)->maxLength(255);
		$this->addText("Apellidos","lastname")->strLimit(150)->maxLength(255);*/
		$this->addText("Estado Civil","civil_status")->strLimit(150)->maxLength(255);
		$this->addText("Atributo Favorito","attribute_id")->strLimit(150)->maxLength(255);
		$this->addText("Tiempo en TV","time_tv")->strLimit(150)->maxLength(255);
		$this->addText("Marca TV","brand_tv")->strLimit(150)->maxLength(255);
		$this->addText("Tamaño Hogar","size_home")->strLimit(150)->maxLength(255);
		$this->addText("Nivel de Satisfacción","satisfy_level")->strLimit(150)->maxLength(255);
		$this->addText("Nro de Personas Hogar","number_home")->strLimit(150)->maxLength(255);
		/*$this->addDatetime("Creado el","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Actualizado el","updated_at")->required(false)->showAdd(false)->showEdit(false);*/

    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personas extends Model
{
    //
    protected $table = "Personas";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "fullname",
        "birthdate",
        "dni",
        "phone",
        "department",
        "city",
        "district",
        "address",
        "email",
        "interest_id",
        "interest_size",
        'name',
        'lastname',
        'civil_status',
        'attribute_id',
        'time_tv',
        'brand_tv',
        'size_home',
        'satisfy_level',
        'number_home'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];    
}

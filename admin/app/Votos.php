<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Votos extends Model
{
    //
    protected $table = "Votos";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','fullmame',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];    
}

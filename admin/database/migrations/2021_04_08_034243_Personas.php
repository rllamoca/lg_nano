<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Personas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Personas', function (Blueprint $table) {
            
            $table->timestamps();
			$table->bigIncrements('id');
			$table->string('fullname')->nullable();
			$table->integer('age')->nullable();
			$table->string('dni', 8)->nullable();
			$table->string('phone', 12)->nullable();
			$table->integer('department')->nullable();
			$table->integer('city')->nullable();
			$table->integer('district')->nullable();
			$table->string('address')->nullable();
			$table->string('email')->nullable();
			$table->integer('interest_id')->nullable();
			$table->integer('interest_size')->nullable();
			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Personas');
    }
}
